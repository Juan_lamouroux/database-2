USE school;

SELECT * FROM Dicta;
SELECT * FROM Estudiante;
SELECT * FROM Horario;
SELECT * FROM Materia;
SELECT * FROM Notas;
SELECT * FROM Profesor;


-- Insertar m�s datos en Estudiante
INSERT INTO Estudiante VALUES
  (2, 'Pedro', 'Gomez'),
  (3, 'Ana', 'Torres');

-- Insertar m�s datos en Profesor 
INSERT INTO Profesor VALUES 
  (2, 'Carlos', 'Perez'),
  (3, 'Julia', 'Lopez');

-- Insertar m�s datos en Materia
INSERT INTO Materia VALUES
  (2, 'F�sica'),
  (3, 'Qu�mica');
  
-- Insertar m�s datos en Dicta  
INSERT INTO Dicta VALUES
  (1, 1), 
  (2, 2), 
  (3, 3); 
  
-- Insertar m�s datos en Horario
INSERT INTO Horario VALUES
  (1, '2020-09-01', '08:00:00', '09:30:00', 1),
  (2, '2020-09-02', '10:00:00', '11:30:00', 2),
  (3, '2020-09-02', '13:00:00', '14:30:00', 3);
  
-- Insertar m�s datos en Notas
INSERT INTO Notas VALUES
  (2, 3.2, 2, 2), 
  (3, 4.5, 3, 3);




  -- Crear vista indexada de estudiantes
CREATE VIEW vw_estudiantes
AS
SELECT * FROM Estudiante

Select * FROM vw_estudiantes;

-- Modificar vista para mostrar solo nombres 
ALTER VIEW vw_estudiantes 
AS
SELECT Id , Nombre FROM Estudiante

Select * FROM vw_estudiantes;

-- Actualizar tabla Estudiante desde la vista
UPDATE vw_estudiantes 
SET Nombre = 'Julio'
WHERE Id = 1;

Select * FROM vw_estudiantes;

-- Eliminar vista
DROP VIEW vw_estudiantes;


-- Creacion de procedimiento para filtrar estudiantes dependiendo del apellido.
CREATE PROCEDURE sp_get_estudiantes 
@Apellido varchar(50)
AS
SELECT * FROM Estudiante
WHERE Apellido = @Apellido

--ejecucion del procedimiento
EXEC sp_get_estudiantes @Apellido = 'Torres';


CREATE PROCEDURE sp_insert_estudiante
@Id int,
@Nombre varchar(50),
@Apellido varchar(50)
AS
IF NOT EXISTS (SELECT 1 FROM Estudiante WHERE Id = @Id)
   INSERT INTO Estudiante (Id,Nombre, Apellido)
   VALUES (@Id, @Nombre, @Apellido)
Else
	PRINT 'Ya hay un Estudiante con este indice';

EXEC sp_insert_estudiante @Id = 4, @Nombre = 'Ana', @Apellido = 'Peralta';
SELECT * FROM Notas

--Aumento de notas dependiendo del incremento que se busque, esto afecta a todas las notas.
CREATE PROCEDURE Aumentar_Notas
@Aumento Decimal(5,3)
AS
DECLARE @id INT
DECLARE @nota DECIMAL(5,3)

SET @id = 1

WHILE @id <= (SELECT MAX(Id) FROM Notas)
BEGIN
  SELECT @nota = Nota FROM Notas WHERE Id = @id
  
  SET @nota = @nota + @Aumento

  UPDATE Notas 
  SET Nota = @nota
  WHERE Id = @id

  SET @id = @id + 1
END

--Ejecucion de las notas
EXEC Aumentar_Notas @Aumento = 0.5


--cambio de nombre, si el nombre materia tiene un nulo como parametro entrara al catch.
CREATE PROCEDURE Cambio_NombreMateria
@NameMateria varchar(20), @Id int AS
BEGIN TRY
	UPDATE Materia 
	SET NombreMateria = @NameMateria
	WHERE Id = @Id;
END TRY
BEGIN CATCH
	SELECT 'Verifica los valores solicitados' AS ErrorMessage;
END CATCH

--ejecucion cambio de nombreMateria
EXEC Cambio_NombreMateria @NameMateria = null, @Id = 2;


--Creamos una tabla notas viejas para almacenar las notas aqui
CREATE TABLE OldNotas (
    Id int  NOT NULL,
    Nota decimal(5,3)  NOT NULL,
    IdEstudiante  int  NOT NULL,
    IdMateria  int  NOT NULL,
    CONSTRAINT OldNotas_pk PRIMARY KEY  (Id)
	)

--Creamos la vista de esas notas viejas ya que sin una tabla no se puede crear una vista.
CREATE VIEW vista_oldNotas
AS
SELECT * FROM OldNotas

CREATE PROCEDURE Migracion_Notas AS
BEGIN
	INSERT INTO vista_oldNotas
	SELECT * FROM Notas
    DELETE FROM Notas
END


EXEC Migracion_Notas;
SELECT * FROM Notas;
SELECT * FROM vista_oldNotas;


CREATE TABLE Cambios_Estudiante (
	Id int PRIMARY KEY IDENTITY(1,1),
	Proceso VARCHAR(50),
	Suceso DATETIME); 

-- creacion del trigger que registra cambios en la tabla estudiante.
CREATE TRIGGER Tr_Registro_Estudent
ON Estudiante
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @Operacion VARCHAR(50);

    IF EXISTS (SELECT * FROM inserted)
    BEGIN
        IF EXISTS (SELECT * FROM deleted)
            SET @Operacion = 'UPDATE';
        ELSE
            SET @Operacion = 'INSERT';
    END
    ELSE
        SET @Operacion = 'DELETE';

    INSERT INTO Cambios_Estudiante VALUES (@Operacion, GETDATE());
END;

--Pruebas trigger
INSERT INTO Estudiante VALUES
  (5, 'julio', 'cesar');

UPDATE Estudiante 
SET Nombre = 'Pedro'
WHERE Id = 5;

DELETE Estudiante FROM Estudiante where ID = 5;

--Cambios en las tablas.
SELECT * FROM Cambios_Estudiante;
SELECT * FROM Notas;

--Creacion de trigger que valida las notas al momento de ser ingresadas.
CREATE TRIGGER Tr_ValidadorNotas ON Notas 
INSTEAD OF INSERT
AS
BEGIN
	-- Verificar si la nota es mayor o igual a cero
 IF (SELECT COUNT(*) FROM inserted WHERE Nota < 0 OR Nota > 100) > 0
  BEGIN
	 -- cancelacion de la operacion y mostrar un mensaje de error.
     THROW 50000, 'La nota debe ser mayor o igual a cero.', 1;
	 RETURN
  END
 ELSE
  BEGIN
	INSERT INTO Notas (Id, Nota, IdEstudiante,IdMateria)
  SELECT Id, Nota, IdEstudiante, IdMateria
  FROM inserted
  END
END

INSERT INTO Notas VALUES (6,1,1,1);
INSERT INTO Notas VALUES (7,-1,1,1);
SELECT * FROM Notas;

--creacion de tabla que guardara los cambios que se hagan en la tabla profesor 
CREATE TABLE tabla_registros_Profesor (
  id INT IDENTITY(1,1) PRIMARY KEY,
  usuario VARCHAR(100),
  operacion VARCHAR(100),
  fecha DATETIME
)

-- Crear trigger que verificara las modificaciones en la tabla de profesores.
CREATE TRIGGER tr_modifi_profesor 
ON Profesor
FOR INSERT, UPDATE, DELETE
AS BEGIN
	DECLARE @Operacion VARCHAR(100);

    IF EXISTS (SELECT * FROM inserted)
    BEGIN
        IF EXISTS (SELECT * FROM deleted)
            SET @Operacion = 'UPDATE';
        ELSE
            SET @Operacion = 'INSERT';
    END
    ELSE
        SET @Operacion = 'DELETE';

  INSERT INTO tabla_registros_Profesor
  (
    usuario,
    operacion,
    fecha
  )
  VALUES
  (
    CURRENT_USER,
    @Operacion, 
    GETDATE()
  )
END

Select * from Profesor

Update Profesor Set Apellido = 'gonzales' where Id = 3;

Select * from tabla_registros_Profesor;



--Creamos procedimiento para insertar un estudiante
CREATE PROCEDURE CrearEstudiante
@Id INT, @Name VARCHAR(50), @Apellido VARCHAR(50) AS
BEGIN
INSERT INTO Estudiante VALUES (@Id,@Name,@Apellido)
END

--Creamos procedimiento para eliminar a un estudiante
CREATE PROCEDURE BorrarEstudiante
@Id INT AS
BEGIN
DELETE Estudiante FROM Estudiante WHERE Id = @Id
END

Select * From vw_estudiantes