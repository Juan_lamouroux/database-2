package org.example;

import java.sql.*;
public class Conexion_DB {
  private Statement statement;
  private Connection connection;

  public Conexion_DB() {
    try {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      String url = "jdbc:sqlserver://localhost;databaseName=school;encrypt=false;trustServerCertificate=true";
      String User = "sa";
      String Password = "Jala";
      connection = DriverManager.getConnection(url, User ,Password);
      statement = connection.createStatement();
      System.out.println("Conexion creada con exito\n");
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  public void createStudent(int id, String nombre, String apellido) throws SQLException {
    CallableStatement cs = this.connection.prepareCall("{call CrearEstudiante(?,?,?)}");

    cs.setInt(1, id);
    cs.setString(2, nombre);
    cs.setString(3, apellido);
    cs.executeUpdate();

  }

  public void obtenerEstudiantes() throws SQLException {
    ResultSet resultSet = this.statement.executeQuery("SELECT * FROM vw_estudiantes;");
    System.out.println("Lista de Estudiantes");
    while (resultSet.next()) {
      System.out.println(resultSet.getInt("Id") + " : " + resultSet.getString("Nombre") + " - "
        + resultSet.getString("Apellido"));
    }
  }

  public void obtenerNotas() throws SQLException {
    ResultSet resultSet = this.statement.executeQuery("SELECT * FROM Notas;");
    System.out.println("Lista de notas");
    while (resultSet.next()) {
      System.out.println(resultSet.getInt("Id") + " : " + resultSet.getFloat("Nota") + " - "
        + resultSet.getInt("IdEstudiante") + " - " + resultSet.getInt("IdMateria"));
    }
  }

  public void actualizarNota(float aumento) throws SQLException {
    CallableStatement cs = this.connection.prepareCall("{call Aumentar_Notas(?)}");
    cs.setFloat(1, aumento);
    cs.executeUpdate();
  }

  public void borrarEstudiante(int id) throws SQLException {
    CallableStatement cs = this.connection.prepareCall("{call BorrarEstudiante(?)}");
    cs.setFloat(1, id);
    cs.executeUpdate();
  }

}
