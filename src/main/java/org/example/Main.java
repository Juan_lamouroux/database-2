package org.example;

import java.sql.SQLException;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
  public static void main(String[] args) throws SQLException {
    // Press Alt+Intro with your caret at the highlighted text to see how
    // IntelliJ IDEA suggests fixing it.
      Conexion_DB conexion = new Conexion_DB();

      conexion.obtenerEstudiantes();
      conexion.createStudent(6,"julian", "jaramillo");
      conexion.obtenerEstudiantes();
      conexion.obtenerNotas();
      conexion.actualizarNota(1);
      conexion.obtenerNotas();
      conexion.borrarEstudiante(6);
      conexion.obtenerEstudiantes();
    }
  }